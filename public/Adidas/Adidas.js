window.onload = function(){
    init();
};

function init(){
    var Usernav = document.getElementById("usernav");
    firebase.auth().onAuthStateChanged(function (user) {
        if(user){
            
            Usernav.innerHTML = '<li class="nav-item"><a class="nav-link">Hello~ ' + user.email + '</a></li><li class="nav-item"><a class="nav-link" href="../sell.html">Seller</a></li><li class="nav-item"><a class="nav-link" href="../index.html" id="logout">Log Out</a></li><li class="nav-item"><a class="nav-link" href="../shopping_cart.html"><i class="fas fa-shopping-cart"></i></a></li>';
            var Logout = document.getElementById("logout");
            Logout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(){
                    alert("Success Sign Out");
                }).catch(function(){
                    alert("Sign Out Error");
                });
            });
        }
        else{
            Usernav.innerHTML = '<li class="nav-item"><a class="nav-link" href="../register.html">Register</a></li><li class="nav-item"><a class="nav-link" href="../signin.html">Log in</a></li>';
        }
    });
    var str_card_before_img = '<div class="card bg-light"><div class="card-header">Adidas</div><img class="card-img-top" src="';
    var str_card_before_name = '"><div class="card-body"><h4 class="card-title">';
    var str_card_before_price = '</h4><p class="card-text">';
    var str_card_before_id = '</p><button type="button" class="btn btn-success" onclick="myFunc(this.id)" id="';
    var str_card_end = '">Buy it Now!</button></div></div>';

    var total_product = [];
    
    firebase.database().ref('product/Adidas').once('value').then(function(snapshot){
        snapshot.forEach(function(element){
            var img = firebase.storage().ref(element.val().img);
            img.getDownloadURL().then(function(url){
                total_product.push(str_card_before_img + url + str_card_before_name + element.val().name + str_card_before_price + element.val().price + str_card_before_id + element.key + str_card_end);
                document.getElementById("myCardDeck").innerHTML = total_product.join('');//must put here
            });
        });
    });
}

var Cuser;
function myFunc(id){
    Cuser = firebase.auth().currentUser;
    if(Cuser){
        //Put this product into this user's cart
        var email = Cuser.email;
        var loc = email.search("@");
        var email2 = email.slice(0 ,loc);
        firebase.database().ref('users/'+email2+'/cart').push({
            product: id
        });

        //Read product data from firebase
        loc = id.search("_");
        var title = id.slice(0, loc);
        firebase.database().ref('product/'+title+'/'+id).once('value').then(function(snapshot){
            alert("Put "+ snapshot.val().name + " into shopping cart.");
        });
        
    }else{
        alert("Please Sign In First!!!");
        window.location.replace("../signin.html");
    }
}