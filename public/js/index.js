window.onload = function(){
    init();
};

function init(){
    var Usernav = document.getElementById("usernav");
    firebase.auth().onAuthStateChanged(function (user) {
        if(user){
            //<li class="nav-item"><a class="nav-link" href="sell.html">Seller</a></li>
            Usernav.innerHTML = '<li class="nav-item"><a class="nav-link">Hello~ ' + user.email + '</a></li><li class="nav-item"><a class="nav-link" href="sell.html">Seller</a></li><li class="nav-item"><a class="nav-link" href="index.html" id="logout">Log Out</a></li><li class="nav-item"><a class="nav-link" href="shopping_cart.html"><i class="fas fa-shopping-cart"></i></a></li>';
            var Logout = document.getElementById("logout");
            Logout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(){
                    alert("Success Sign Out");
                }).catch(function(){
                    alert("Sign Out Error");
                });
            });
        }
        else{
            Usernav.innerHTML = '<li class="nav-item"><a class="nav-link" href="register.html">Register</a></li><li class="nav-item"><a class="nav-link" href="signin.html">Log in</a></li>';
        }
        //Just for the first time set some basic product
        /*
        var ProductRef = firebase.database().ref('product');
        ProductRef.set({
            Nike: {
                Nike_product1:{
                    img: 'img/Nike_product1.jpg',
                    name: 'Nike Air Max 98 On Air Gabrielle Serrano',
                    price: '$200',
                    description: 'The Nike Air Max 98 On Air Gabrielle Serrano brings back retro Air Max style with contemporary comfort innovations and a fresh, new design inspired by the community and landmarks of New York City. The durable upper blends textile with leather overlays, while a full-length Max Air unit gives total cushioning in every step.',
                    owner: "admin"
                },
                Nike_product2:{
                    img: 'img/Nike_product2.jpg',
                    name: 'Nike Air Max 720',
                    price: '$180',
                    description: 'The Nike Air Max 720 goes bigger than ever before with Nike’s tallest Air unit yet, offering more air underfoot for unimaginable, all-day comfort. Has Air Max gone too far? We hope so.',
                    owner: "admin"
                },
                Nike_product3:{
                    img: 'img/Nike_product3.jpg',
                    name: 'Nike Air VaporMax 2019',
                    price: '$190',
                    description: 'Designed for running but adopted by the street, the Nike Air VaporMax 2019 features the lightest, most flexible Air Max cushioning to-date. A stretch woven material wraps your foot for lightweight support and stability, while an external reinforcement in the heel secures the back of your foot.',
                    owner: "admin"
                },
                Nike_product4:{
                    img: 'img/Nike_product4.jpg',
                    name: 'LeBron 16 Low',
                    price: '$160',
                    description: "LeBron is unstoppable on the court and just as influential off of it. The year 2003, LeBron's rookie season and a pivotal year in culture, inspires the LeBron 16 Low. It is powered by Max Air and Zoom Air cushioning technologies, and embodies his clout and versatility.",
                    owner: "admin"
                }
            },
            UA:{
                UA_product1:{
                    img: 'img/UA_product1.jpg',
                    name: 'UA Curry 6',
                    price: '$130',
                    description: "FRESH. NEW. INNOVATIVE. GROUNDBREAKING. Positive, right? That’s what they said about Stephen when he first stepped on the court 10 years ago. Changing the game isn’t easy and before long, haters came out of the woodwork. But that’s what happens when you make people think about the game, play the game, and even dream about the game…differently. If that’s ruining the game—then he’ll just need to keep on doing it. NO APOLOGIES. ALL LOVE. COMPLETE COMMITMENT.",
                    owner: "admin"
                },
                UA_product2:{
                    img: 'img/UA_product2.jpg',
                    name: 'UA HOVR Infinite — Wide (4E)',
                    price: '$120',
                    description: "When we set out to develop the best neutral running shoe, we asked pro distance runners what they needed. Their response: cushion, bounce, durability, and efficiency. Fast-forward to the super-smooth ride of UA HOVR Infinite. Perfect for everything from your long runs to laying base mileage, and even recovery mileage days. As soon as you put them on, you'll feel like you can run forever.",
                    owner: "admin"
                },
                UA_product3:{
                    img: 'img/UA_product3.jpg',
                    name: 'Project Rock 1',
                    price: '$120',
                    description: '“Project Rock is not a brand, it’s a movement. It’s a core belief, that I 100% don’t care what color you are, how old you are, where you come from or what you do for a living. The only thing I care about is you and me, building the belief that regardless of whatever the odds, we can overcome and achieve—but it all starts with the work we’re willing to put in with our two hands."—Dwayne Johnson',
                    owner: "admin"
                },
                UA_product4:{
                    img: 'img/UA_product4.jpg',
                    name: 'UA TriBase Reign',
                    price: '$120',
                    description: "When you hit the gym, you need to feel as connected with floor as possible. Every lift, rep, and WOD starts from the ground up. UA TriBase technology maximizes ground control with three points of contact for a low, stable base that still allows natural foot flexibility.",
                    owner: "admin"
                }
            },
            Adidas:{
                Adidas_product1:{
                    img: 'img/Adidas_product1.jpg',
                    name: 'ADIZERO ADIOS 4 SHOES',
                    price: '$140',
                    description: 'Master Japanese shoemaker Omori designed these neutral shoes to be worn by the top marathon runners in the world. They have a locked-down fit for racing and fast training. Responsive cushioning returns energy with every step. The ultra-lightweight and breathable mesh upper offers support with a barely there feel.',
                    owner: "admin"
                },
                Adidas_product2:{
                    img: 'img/Adidas_product2.jpg',
                    name: 'ADIZERO PRIME LTD SHOES',
                    price: '$180',
                    description: 'Made for fast training on the road or the track, these neutral running shoes have a lightweight knit upper that adapts to the shape of your foot as you run. The dual-lacing system allows you to personalize the fit for precise lockdown. Responsive cushioning keeps your stride energized.',
                    owner: "admin"
                },
                Adidas_product3:{
                    img: 'img/Adidas_product3.jpg',
                    name: 'SOLAR RIDE SHOES',
                    price: '$100',
                    description: "You build conditioning one mile, one run, one race at a time. Designed for long distances, these shoes have a mesh upper with stitched-in reinforcement for targeted support. Flexible cushioning helps keep a spring in your step from start to finish.",
                    owner: "admin"
                },
                Adidas_product4:{
                    img: 'img/Adidas_product4.jpg',
                    name: 'ULTRABOOST 19 SHOES',
                    price: '$180',
                    description: "Running reinvented. These high-performance neutral running shoes deliver unrivaled comfort and energy return. The lightweight and propulsive shoes have a seamless knit upper that's engineered with motion weave technology to provide stretch while also holding your foot in place as you run. The second-skin fit follows the shape of your foot to reduce pressure points.",
                    owner: "admin"
                }
            }
        });*/
    });

}

var Cuser;
$(document).ready(function(){
$("#Nike_product1, #Nike_product2, #Nike_product3, #Nike_product4, #UA_product1, #UA_product2, #UA_product3, #UA_product4 ,#Adidas_product1, #Adidas_product2, #Adidas_product3 ,#Adidas_product4").on('click', function(){
    Cuser = firebase.auth().currentUser;
    if(Cuser){
        //Put this product into this user's cart
        var email = Cuser.email;
        var loc = email.search("@");
        var email2 = email.slice(0 ,loc);
        firebase.database().ref('users/'+email2+'/cart').push({
            product: this.id
        });

        //Read product data from firebase
        loc = this.id.search("_");
        var title = this.id.slice(0, loc);
        firebase.database().ref('product/'+title+'/'+this.id).once('value').then(function(snapshot){
            alert("Put "+ snapshot.val().name + " into shopping cart.");
        });
        
    }else{
        alert("Please Sign In First!!!");
        window.location.replace("signin.html");
    }
});
});