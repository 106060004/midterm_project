window.onload = function(){
    init();
};

var usere;

function init(){
    var Usernav = document.getElementById("usernav");
    var useremail;
    firebase.auth().onAuthStateChanged(function (user) {
        if(user){
            useremail = user.email;
            Usernav.innerHTML = '"<li class="nav-item"><a class="nav-link">Hello~ ' + useremail + '</a></li><li class="nav-item"><a class="nav-link" href="sell.html">Seller</a></li><li class="nav-item"><a class="nav-link" href="index.html" id="logout">Log Out</a></li><li class="nav-item"><a class="nav-link" href="shopping_cart.html"><i class="fas fa-shopping-cart"></i></a></li>';
            var Logout = document.getElementById("logout");
            Logout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(){
                    alert("Success Sign Out");
                }).catch(function(){
                    alert("Sign Out Error");
                });
            });
            var str_card_before_title = '<div class="mt-3 border d-flex"><div class="mx-3" style="width:50px"><h6>';
            var str_card_before_img = '</h6></div><img class="mx-3" width="100px" height="100px" src="';
            var str_card_before_name = '"><div class="my-auto mr-auto"><h5 class="mx-2">';
            var str_card_before_price = '</h5></div><div><p class="mx-3">';
            var str_card_before_id = '</p></div><div><button type="button" onclick="myFunc(this.id)" class="btn btn-danger" id="';
            var str_card_end = '">Delete!</button></div></div>';
            var str_end = '<button type="button" class="btn btn-info mt-4 " style="float:right" id="myOrder" onclick="mySubmitDunc()">Submit</button>';

            var total_cart = [];
            var loc = useremail.search("@");
            usere = useremail.slice(0 ,loc);
    
            firebase.database().ref('users/'+usere+'/cart').once('value').then(function(snapshot){
                snapshot.forEach(function(element){
                    var productID = element.val().product;
                    loc = productID.search("_");
                    var title = productID.slice(0, loc);
            
                    firebase.database().ref('product/'+title+'/'+productID).once('value').then(function(snapshot){
                        var img = firebase.storage().ref(snapshot.val().img);
                        img.getDownloadURL().then(function(url){
                            total_cart.push(str_card_before_title + title + str_card_before_img + url + str_card_before_name + snapshot.val().name + str_card_before_price + snapshot.val().price + str_card_before_id + element.key + str_card_end );
                            document.getElementById("myContent").innerHTML = total_cart.join('')+str_end;
                        });
                    });
                });
            });
        }
        else{
            useremail = '';
            Usernav.innerHTML = '<li class="nav-item"><a class="nav-link" href="register.html">Register</a></li><li class="nav-item"><a class="nav-link" href="signin.html">Log in</a></li>';
        }
    });   
}

function myFunc(id){
    firebase.database().ref('users/'+usere+'/cart/'+id).remove();
    window.location.replace('shopping_cart.html');
}

function mySubmitDunc(){
    firebase.database().ref('users/'+usere+'/cart').remove();
    //send request to seller
    alert("You have buy these things successfully");
    window.location.replace('index.html');
}