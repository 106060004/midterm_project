window.onload = function(){
    initApp();
};

function initApp(){
    txtUsername = document.getElementById("inputUsername");
    Email = document.getElementById("inputEmail");
    Password = document.getElementById("inputPassword");
    Policy = document.getElementById("customCheck");

    birth_D = document.getElementById("birth_D");
    birth_M = document.getElementById("birth_M");
    birth_Y = document.getElementById("birth_Y");

    //there is some bug here
    $("#myResgister").on('click', function(){
        if(Policy.checked){//bug here
            //creat this user's database
            var emailref = Email.value;
            var loc = emailref.search("@");
            var emailref2 = emailref.slice(0, loc);
            firebase.database().ref('users/'+emailref2).set({
                username: txtUsername.value,
                email: Email.value,
                birthday: birth_Y.value.toString()+'Y'+birth_M.value.toString()+'M'+birth_D.value.toString()+'D'
            });
            firebase.auth().createUserWithEmailAndPassword(Email.value, Password.value).then(function(success){
                window.location.replace("index.html");
            }).catch(function(error){
                create_alert('error', error.message);
            });
        }
    });

    //Facebook and Google
    $("#mySigninFacebook").on('click', function(){
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            
            //console.log(user);
            var emailref = user.email;
            var loc = emailref.search("@");
            var emailref2 = emailref.slice(0, loc);
            firebase.database().ref('users/'+emailref2).set({
                username: user.displayName,
                email: user.email
            }, function(){
                window.location.replace("index.html");
            });
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            create_alert('error', error.message);
        });
    });
    $("#mySigninGoogle").on('click', function(){
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...
            //console.log(user);
            var emailref = user.email;
            var loc = emailref.search("@");
            var emailref2 = emailref.slice(0, loc);
            firebase.database().ref('users/'+emailref2).set({
                username: user.displayName,
                email: user.email
            },function(){
                window.location.replace("index.html");
            });
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
            create_alert('error', error.message);
        });
    });
}

function create_alert(type, message){
    var div = document.getElementById("alert_area");
    if(type == "success"){
        div.innerHTML = '<div class="alert alert-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success!</strong>'+message+'</div>';
    }else if(type == "error"){
        div.innerHTML = '<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong>'+message+'</div>';
    }
}