$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

window.onload = function(){
    init();
};

function init(){
    var Usernav = document.getElementById("usernav");
    firebase.auth().onAuthStateChanged(function (user) {
        if(user){
            
            Usernav.innerHTML = '<li class="nav-item"><a class="nav-link">Hello~ ' + user.email + '</a></li><li class="nav-item"><a class="nav-link" href="index.html" id="logout">Log Out</a></li><li class="nav-item"><a class="nav-link" href="shopping_cart.html"><i class="fas fa-shopping-cart"></i></a></li>';
            var Logout = document.getElementById("logout");
            Logout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(){
                    alert("Success Sign Out");
                }).catch(function(){
                    alert("Sign Out Error");
                });
            });
            document.getElementById("mySellCart").innerHTML = '<h4>Or you can go to see what is in your sell cart</h4><a href="sell_cart.html" class="btn btn-secondary">Sell Cart</a>';
        }
        else{
            Usernav.innerHTML = '<li class="nav-item"><a class="nav-link" href="register.html">Register</a></li><li class="nav-item"><a class="nav-link" href="signin.html">Log in</a></li>';
        }
        $("#mySubmit").on('click', function(){
            if(user){
                var title = document.getElementById("productTitle").value;
                var name = document.getElementById("productName").value;
                var img = document.getElementById("productImg");//couldn't upload img
                var description = document.getElementById("productDes").value;
                var price = document.getElementById("productPrice").value;

                var loc = user.email.search("@");
                usere = user.email.slice(0 ,loc);
                
                var count=0;
                firebase.database().ref('product/'+title).once('value').then(function(snapshot){
                    snapshot.forEach(function(element){
                        count++;
                    });
                    count++;
                    var productID = title+'_product'+count;

                    //deal with img
                    //But this is not stable.Sometimes I need to wait too long.
                    firebase.storage().ref('img/'+productID+'.jpg').put(img.files[0]);
                    

                    firebase.database().ref('users/'+usere+'/seller').push({
                        sell: productID
                    });
                    firebase.database().ref('product/'+title+'/'+productID).set({
                        description: description,
                        img: 'img/'+productID+'.jpg',
                        name: name,
                        price: '$'+price,
                        owner: usere
                    });
                    window.location.replace("sell_cart.html");
                })
                
            }else{
                alert("Please Sign In First");
                window.location.replace("signin.html");
            }
        });
    });
}