# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Shopping Web Page
* Key functions (add/delete)
    1. init
    2. initApp
    
* Other functions (add/delete)
    1. firebase.database()
    2. firebase.storage()

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-project-8faee.firebaseapp.com

# Components Description : 
1. 首頁index: 
首先剛開始連結到首頁，雖然一開始創了各個頁面的css，但大多是利用bootstrap完成排版，所以也有RWD。如果在這裡試著購買商品，則會跳出先登入的通知。註：最上面的三張圖片並沒有找到商品資訊，所以沒有做連結。另外，search功能並沒有實現!!!!!
然後若是在未登入的狀況跳到其他頁面也是看不到東西的，只能先給沒有權限的瀏覽者大致看一下商品內容XD

2. 登入/註冊signin/register: 
接下來就是登入的頁面，登入跟註冊會互相有連結讓使用者挑選，上面的功能都是可以使用的，不過facebook雖然有註冊project，不過因為還在開發狀態所以只有我自己的帳號可以登入，害我浪費一堆時間= =

3. 登入後的頁面: 
登入之後，利用js裡面innerHTML改變navbar的內容，讓有登入的使用者可以看到基本的logout，還有seller跟cart的功能。還有可以看到分別Nike、UnderArmour、Adidas的頁面。
註：分類這樣分一開始以為是像蝦皮一樣使用者也可以拍賣，單純只是讓消費者瀏覽。
而這三個頁面就會從database以及storage讀資料來顯示了。也可以直接點選"Buy it now"的button來加入購物車。

4. 賣家sell: 這裡會先導向一個填入想拍賣的商品資料。也可以點選左下角的button直接去看你現在拍賣的什麼東西。
註：分類的部分上面解釋過了，雖然有點怪不過就當作三間大公司來這邊賣東西...?

5. 拍賣車sell_cart/購物車shopping_cart: 因為兩者相近，所以放在這裡介紹，兩者都會在database裡面有一個分支，再push的方式紀錄購買的商品以及拍賣的商品，delete時就可以利用pushID找到要刪掉的商品。

6. database裡面大概的介紹:先分成兩個，product跟users。
6.1 product 分成3類：Nike、UA、Adidas，接下來以Nike為例，每個商品分別為Nike_produc'x'，'x'為數字，會計算給每個商品算是編號，來表示每個商品，而每個商品裡面再記錄了：description、img、name、owner、price。
註：img為storage的位置
6.2 users則是利用@以前的email名字命名，每個user裡面有基本資料例如：birthday、name、email，還有兩個push用的分支，一個為seller，另一個為cart，分別記錄拍賣的商品跟購買的商品。

# Other Functions Description(1~10%) : 
這邊打得比較像心得
1. 首先是因為利用firebase，所以不太可能跑的比電腦執行速度快，所以一開始如果要用addEventListener或是JQuery會一直找不到前面用innerHTM改變的button，替代方案是利用onclick直接新創一個function來負責要做的事。

2. 因為都要從database跟storage讀資料，所以可以說幾乎每個頁面都必須利用js改變其值，所以都要用innerHTML去改變其值，所以就會要用許多string讓code變得較為複雜。還有資料上的處理也需要很多的字串處理，例如email要先去掉@之後的部分等等。

3. 如果登入/註冊畫面有誤會有fade的alert，算是CSS animation...?不過做這次的作業大概把Bootstrap的所有功能都看了，這應該是最花時間的，不過也真的很好用，幾乎都不用自己寫css了。

4. 寫購物網站感覺實在是麻煩許多，抱怨如下XD：
4-1. 光一開始就要蒐集許多資料不然網站什麼都沒有
4-2. 因為資料很多的關係，再自己本地端的分類就變得檔案很多而且雜亂，若是想要導到另外一個網站就必須一直檢查data path。
4-3. 一定要去找storage的資料，因為拍賣一定要有圖片阿，雖然後來知道跟database差不多，但是因為可能delay的關係一開始研究了很久。
不過可以玩比較多好看的東西，bootstrap的很多東西都會在這邊用到。


## Security Report (Optional)
算是基本的概念，即使沒有登入也可以讀商品的資料，但登入之後才可以處理其他資料。
